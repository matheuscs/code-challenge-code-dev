import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'any'
})
export class UserAccountService {

  headers = new HttpHeaders({
    'Accept-Language': "en"
  });

  private baseUrl = 'http://localhost:8081/api/management';
  private v1_userAccounts = '/v1/user-accounts';

  constructor(private http: HttpClient) { }

  getAllUserAccounts(): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl + this.v1_userAccounts);
  }

  getUserAccountById(uuid: string): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl + this.v1_userAccounts + "/ed3afa15-0246-41c2-98d8-3dfb6501f185", { headers: this.headers });
  }

}
