import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {UserAccountListComponent} from "../user-account-list/user-account-list.component";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, UserAccountListComponent, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'Matheus test';
}
