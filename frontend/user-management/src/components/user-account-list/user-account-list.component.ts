import { Component } from '@angular/core';
import { UserAccountService } from '../../services/user-account/user-account.service';
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";

@Component({
  selector: 'app-user-account-list',
  standalone: true,
  imports: [CommonModule, HttpClientModule],
  templateUrl: './user-account-list.component.html',
  styleUrl: './user-account-list.component.scss'
})
export class UserAccountListComponent {

  users: any[] = [];

  constructor(private userService: UserAccountService) { }

  ngOnInit(): void {
    this.loadAllUserAccounts();
    this.loadUserAccountById("ed3afa15-0246-41c2-98d8-3dfb6501f185");
  }

  loadAllUserAccounts(): void {
    this.userService.getAllUserAccounts().subscribe({
      next: (data) => {
        this.users = data
      },
      error: (error) => {
        console.error('Erro ao carregar usuários: ', error)
      }
    });
  }

  loadUserAccountById(uuid: string): void {
    this.userService.getUserAccountById(uuid).subscribe({
      next: (data) => {
        console.log('id', data)
      },
      error: (error) => {
        console.error('Erro ao carregar usuários: ', error)
      }
    });
  }



}
