# Processo seletivo desenvolvedor fullstack Java/Angular

##### Requisitos técnicos obrigatórios
- [x] Java
- [x] Spring Boot
- [ ] API/Webservices REST
- [ ] JUnit
- [ ] Angular 17 / TypeScript
- [x] Banco de dados Postgres;
- [x] Docker
- [ ] GitLab / CI/CD;
- [ ] AWS
- [x] Metodologias ágeis;

## > > Desafio: Sistema de Gerenciamento de Usuários < <

### Contexto - Geral
- [ ] Este sistema deve permitir que os administradores realizem operações CRUD (Criar, Pesquisar, Atualizar e Deletar) usuários.

### Requisitos Técnicos - Específico

1. [x] **Java 17 ou superior**: O backend do sistema deve ser desenvolvido em Java, utilizando as funcionalidades mais recentes disponíveis.
2. [ ] **Angular 17**: A interface de usuário deve ser construída utilizando Angular 17, com operações CRUD para gerenciar usuários.
3. [x] **JPA e Hibernate**: Utilize JPA (Java Persistence API) e Hibernate para mapeamento objeto-relacional e realizar operações de banco de dados.
4. [x] **Flyway**: Utilize o Flyway para gerenciar a migração do esquema do banco de dados. Certifique-se de que o esquema do banco de dados seja versionado e mantenha-se consistente com as alterações no código.
5. [x] **Spring Boot**: Utilize o Spring Boot para configurar e desenvolver o backend do sistema. Aproveite ao máximo as convenções e funcionalidades oferecidas pelo Spring Boot.
6. [x] **Controle de Exceções**: Implemente um controle de exceções para validar e tratar os dados fornecidos pelo usuário. Certifique-se de fornecer mensagens de erro significativas e tratamentos adequados para situações inesperadas.
7. [ ] **Casos de Uso de Usuários**: Implemente os seguintes casos de uso relacionados a usuários:
    - [ ] Backend: 
      - [ ] Cadastro de novo usuário.
      - [ ] Listagem de usuários cadastrados.
      - [ ] Visualização detalhada de um usuário específico.
      - [ ] Atualização dos dados de um usuário.
      - [ ] Exclusão de um usuário.

   - [ ] Frontend:
      - [ ] Cadastro de novo usuário.
      - [ ] Listagem de usuários cadastrados.
      - [ ] Visualização detalhada de um usuário específico.
      - [ ] Atualização dos dados de um usuário.
      - [ ] Exclusão de um usuário.

8. [ ] **Entidade Departamento**:
   - [ ] Backend:
      - [ ] Implemente uma entidade "Departamento" para separar os usuários no sistema por departamento. 
      - [ ] Cada usuário deve ser associado a um único departamento.

   - [ ] Frontend:
     - [ ] Implemente uma entidade "Departamento" para separar os usuários no sistema por departamento.
     - [ ] Cada usuário deve ser associado a um único departamento.c

### Requisitos Adicionais e Uso de Outras Tecnologias
- [x] Lombok
- [x] Internationalization with MessageSource
- [x] Error Handler com ControllerAdvice
- [ ] Documentacao com https://app.diagrams.net/
- [ ] Notification Service utilizando Kafka broker
- [ ] Utilização de Clean Architecture ou Ports And Adapters
- [ ] Unit Tests with H2 database
- [ ] Deploy na AWS com EKS
- [ ] Aplicação de Patterns

### Entrega

- [ ] O código-fonte do projeto deve ser entregue em um repositório Git (por exemplo, GitHub, GitLab, Bitbucket) e enviado para o email codedevelop.contato@gmail.com, envie também nesse email o seu curículo atualizado.
- [ ] É uma boa prática fazer commits de forma incremental durante o desenvolvimento, fornecendo uma narrativa clara da evolução do projeto.

### Critérios de Avaliação

1. **Funcionalidade Completa**: 
   - [ ] O sistema deve funcionar conforme os requisitos especificados, permitindo que os usuários realizem todas as operações CRUD de forma eficiente.
2. **Qualidade do Código**: 
   - [ ] O código deve ser limpo, modular e seguir as melhores práticas de desenvolvimento em Java e Angular.
     - Deve ser facilmente compreensível e passível de manutenção.
3. **Testabilidade**: 
   - [ ] O código deve ser testável
   - [ ] Tstes unitários
   - [ ] Testes de integração cobrindo as principais funcionalidades do sistema
4. **Documentação**: 
   - [ ] Forneça uma documentação clara e concisa
   - [ ] Descrevendo a arquitetura do sistema 
   - [ ] As tecnologias utilizadas
   - [ ] Instruções para configurar e executar o projeto localmente