package com.notification.kafka.producer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SendNotificationTopicProducerService {

    @Value("${topic.name.producer.communication-send}")
    private String topicName;

//    private final KafkaTemplate<String, SendCommunicationMessage> kafkaTemplate;
//
//    public void execute(SendCommunicationMessage message){
//        log.info("Payload enviado: {}", message);
//        kafkaTemplate.send(topicName, message);
//    }

}