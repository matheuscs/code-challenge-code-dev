package com.notification.kafka.dtos;

import java.io.Serializable;
import java.util.UUID;

public record SendNotificationMessage(
    UUID accountId,
    String emailTo,
    String payload
) implements Serializable {}