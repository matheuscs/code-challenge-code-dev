package com.notification.kafka.dtos;

import java.util.UUID;

public record NotificationSentMessage(
    UUID id,
    UUID accountId,
    String emailTo,
    String payload,
    String response
) {}