package com.management.kafka.dtos;

import java.io.Serializable;
import java.util.UUID;

public record SendCommunicationMessage(
    UUID accountId,
    String emailTo,
    String payload
) implements Serializable {}