package com.management.kafka.dtos;

import java.util.UUID;

public record CommunicationSentMessage(
    UUID id,
    UUID accountId,
    String emailTo,
    String payload,
    String response
) {}