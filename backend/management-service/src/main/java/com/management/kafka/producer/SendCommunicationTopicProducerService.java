package com.management.kafka.producer;

import com.management.domain.usecases.SendCommunicationTopicProducerUseCase;
import com.management.kafka.dtos.SendCommunicationMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SendCommunicationTopicProducerService implements SendCommunicationTopicProducerUseCase {

    @Value("${topic.name.producer.communication-send}")
    private String topicName;

    private final KafkaTemplate<String, SendCommunicationMessage> kafkaTemplate;

    public void execute(SendCommunicationMessage message){
        log.info("Payload enviado: {}", message);
        kafkaTemplate.send(topicName, message);
    }

}