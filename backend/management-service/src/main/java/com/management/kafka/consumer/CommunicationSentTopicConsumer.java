package com.management.kafka.consumer;

import com.management.kafka.dtos.CommunicationSentMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommunicationSentTopicConsumer {

    @Value("${topic.name.consumer.communication-sent}")
    private String topicName;

    @KafkaListener(
        topics = "${topic.name.consumer.communication-sent}",
        groupId = "group_id"
    )
    public void consume(ConsumerRecord<String, CommunicationSentMessage> payload){
        log.info("AccountApplication");
        log.info("Tópico: {}", topicName);
        log.info("key: {}", payload.key());
        log.info("Headers: {}", payload.headers());
        log.info("Partion: {}", payload.partition());
        log.info("Response: {}", payload.value());
    }

}