package com.management.resources;

import com.management.domain.entities.UserAccount;
import com.management.domain.usecases.CreateUserAccountUseCase;
import com.management.domain.usecases.DeleteUserAccountUseCase;
import com.management.domain.usecases.FindAllUserAccountUseCase;
import com.management.domain.usecases.FindUserAccountByIdUseCase;
import com.management.domain.usecases.UpdateUserAccountUseCase;
import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user-accounts")
public class UserAccountResource {

    private final FindAllUserAccountUseCase findAllUserAccountUseCase;

    private final FindUserAccountByIdUseCase findUserAccountByIdUseCase;

    private final CreateUserAccountUseCase createUserAccountUseCase;

    private final UpdateUserAccountUseCase updateUserAccountUseCase;

    private final DeleteUserAccountUseCase deleteUserAccountUseCase;

    @Autowired
    UserAccountResource(
        FindAllUserAccountUseCase findAllUserAccountUseCase,
        FindUserAccountByIdUseCase findUserAccountByIdUseCase,
        CreateUserAccountUseCase createUserAccountUseCase,
        UpdateUserAccountUseCase updateUserAccountUseCase,
        DeleteUserAccountUseCase deleteUserAccountUseCase
    ) {
        this.findAllUserAccountUseCase = findAllUserAccountUseCase;
        this.findUserAccountByIdUseCase = findUserAccountByIdUseCase;
        this.createUserAccountUseCase = createUserAccountUseCase;
        this.updateUserAccountUseCase = updateUserAccountUseCase;
        this.deleteUserAccountUseCase = deleteUserAccountUseCase;
    }

    @GetMapping
    public ResponseEntity<List<UserAccount>> findByAll(){
        return ResponseEntity.ok().body(findAllUserAccountUseCase.execute());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserAccount> findById(
        @PathVariable(value = "id") UUID id,
        @RequestHeader(name = "Accept-Language", required = false) Locale locale
    ){
        return ResponseEntity.ok().body(findUserAccountByIdUseCase.execute(id));
    }

    @PostMapping
    public ResponseEntity<UserAccount> save(
        @RequestBody UserAccount userAccount
    ){
        return ResponseEntity.created(URI.create("/user-account/" + userAccount.getId()))
            .body(createUserAccountUseCase.execute(userAccount));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(
        @PathVariable(value = "id") UUID id,
        @RequestBody UserAccount userAccount
    ){
        updateUserAccountUseCase.execute(userAccount);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(
        @PathVariable(value = "id") UUID id
    ){
        deleteUserAccountUseCase.execute(id);
        return ResponseEntity.noContent().build();
    }

}