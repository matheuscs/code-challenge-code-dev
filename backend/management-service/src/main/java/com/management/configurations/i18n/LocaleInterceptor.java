package com.management.configurations.i18n;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Locale;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

@Component
public class LocaleInterceptor implements HandlerInterceptor {

    public boolean preHandle(
        @NotNull HttpServletRequest request,
        @NotNull HttpServletResponse response,
        @NotNull Object handler
    ) throws IllegalStateException {
        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        if (localeResolver == null)
            throw new IllegalStateException("No LocaleResolver found: not in a DispatcherServlet request?");

        if (localeResolver instanceof AcceptHeaderLocaleResolver headerLocaleResolver)
            Locale.setDefault(headerLocaleResolver.resolveLocale(request));
        else
            throw new IllegalStateException("Resolver should be of AcceptHeaderLocaleResolver type");

        return true;
    }

}