package com.management.configurations;

import com.management.configurations.i18n.LocaleInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import java.util.Locale;

@Configuration
public class I18nConfiguration implements WebMvcConfigurer {

    private static final String MESSAGES_DIRECTORY = "classpath:i18n/messages";
    private static final String MESSAGES_ENCODING = "UTF-8";
    private static final long MESSAGES_CACHE_MILLIS = 3600;

    private final LocaleInterceptor localeInterceptor;

    @Autowired
    I18nConfiguration(
        LocaleInterceptor localeInterceptor
    ){
        this.localeInterceptor = localeInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry interceptorRegistry) {
        interceptorRegistry.addInterceptor(localeInterceptor);
    }

    @Bean
    public AcceptHeaderLocaleResolver acceptHeaderLocaleResolver() {
        AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();
        localeResolver.setDefaultLocale(Locale.ENGLISH);
        return localeResolver;
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename(MESSAGES_DIRECTORY);
        messageSource.setDefaultEncoding(MESSAGES_ENCODING);
        messageSource.setCacheMillis(MESSAGES_CACHE_MILLIS);

        return messageSource;
    }

}