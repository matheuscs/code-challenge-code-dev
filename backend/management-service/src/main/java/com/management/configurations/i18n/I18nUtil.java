package com.management.configurations.i18n;

import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class I18nUtil {

    private final MessageSource messageSource;

    @Autowired
    I18nUtil(
        MessageSource messageSource
    ) {
        this.messageSource = messageSource;
    }

    public String getMessage(String code, String... args){
        return messageSource.getMessage(code, args, Locale.getDefault());
    }

}