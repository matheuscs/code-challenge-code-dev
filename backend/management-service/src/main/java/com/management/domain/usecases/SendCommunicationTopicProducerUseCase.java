package com.management.domain.usecases;

import com.management.kafka.dtos.SendCommunicationMessage;

public interface SendCommunicationTopicProducerUseCase {
    void execute(SendCommunicationMessage communicationMessage);
}
