package com.management.domain.services;

import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.FindUserAccountByIdUseCase;
import com.management.domain.entities.UserAccount;
import com.management.exceptions.EntityNotFoundException;
import com.management.configurations.i18n.I18nUtil;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindUserAccountByIdService implements FindUserAccountByIdUseCase {

    private final I18nUtil i18nUtil;

    private final UserAccountRepository userAccountRepository;

    @Autowired
    FindUserAccountByIdService(I18nUtil i18nUtil, UserAccountRepository userAccountRepository) {
        this.i18nUtil = i18nUtil;
        this.userAccountRepository = userAccountRepository;
    }
    
    @Override
    public UserAccount execute(UUID uuid) {
        return userAccountRepository.findById(uuid).orElseThrow(
            () -> new EntityNotFoundException(
                i18nUtil.getMessage(
                    "exception.entity-not-found",
                    UserAccount.class.getSimpleName()
                )
            )
        );
    }

}
