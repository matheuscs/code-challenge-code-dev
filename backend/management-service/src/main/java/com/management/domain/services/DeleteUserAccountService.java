package com.management.domain.services;

import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.DeleteUserAccountUseCase;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteUserAccountService implements DeleteUserAccountUseCase {

    private final UserAccountRepository userAccountRepository;

    @Autowired
    DeleteUserAccountService(
        UserAccountRepository userAccountRepository
    ) {
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public void execute(UUID userAccountId) {
        userAccountRepository.deleteById(userAccountId);
    }

}