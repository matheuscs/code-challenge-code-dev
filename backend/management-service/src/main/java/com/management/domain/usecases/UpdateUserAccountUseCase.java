package com.management.domain.usecases;

import com.management.domain.entities.UserAccount;

public interface UpdateUserAccountUseCase {
    UserAccount execute(UserAccount userAccount);
}
