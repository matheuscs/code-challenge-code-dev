package com.management.domain.services;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.CreateUserAccountUseCase;
import com.management.domain.usecases.SendCommunicationTopicProducerUseCase;
import com.management.kafka.dtos.SendCommunicationMessage;
import com.management.kafka.producer.SendCommunicationTopicProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateUserAccountService implements CreateUserAccountUseCase {

    private final UserAccountRepository userAccountRepository;

    private final SendCommunicationTopicProducerUseCase sendCommunicationTopicProducerUseCase;

    @Autowired
    CreateUserAccountService(
        UserAccountRepository userAccountRepository,
        SendCommunicationTopicProducerService sendCommunicationTopicProducerService
    ) {
        this.userAccountRepository = userAccountRepository;
        this.sendCommunicationTopicProducerUseCase = sendCommunicationTopicProducerService;
    }

    @Override
    public UserAccount execute(UserAccount userAccount) {
        var savedUserAccount = userAccountRepository.save(userAccount);

        sendCommunicationTopicProducerUseCase.execute(
            new SendCommunicationMessage(
                savedUserAccount.getId(),
                savedUserAccount.getEmail(),
                "Created '" + savedUserAccount.getEmail() + "'"
            )
        );

        return savedUserAccount;
    }
}