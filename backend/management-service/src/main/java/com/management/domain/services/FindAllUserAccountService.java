package com.management.domain.services;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.FindAllUserAccountUseCase;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindAllUserAccountService implements FindAllUserAccountUseCase {

    private final UserAccountRepository userAccountRepository;

    @Autowired
    FindAllUserAccountService(UserAccountRepository userAccountRepository) {
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public List<UserAccount> execute() {
        return userAccountRepository.findAll();
    }

}
