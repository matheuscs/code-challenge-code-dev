package com.management.domain.services;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.UpdateUserAccountUseCase;
import com.management.kafka.dtos.SendCommunicationMessage;
import com.management.kafka.producer.SendCommunicationTopicProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateUserAccountService implements UpdateUserAccountUseCase {

    private final UserAccountRepository userAccountRepository;

    private final SendCommunicationTopicProducerService sendCommunicationTopicProducerService;

    @Autowired
    UpdateUserAccountService(
        UserAccountRepository userAccountRepository,
        SendCommunicationTopicProducerService sendCommunicationTopicProducerService
    ) {
        this.userAccountRepository = userAccountRepository;
        this.sendCommunicationTopicProducerService = sendCommunicationTopicProducerService;
    }

    @Override
    public UserAccount execute(UserAccount userAccount) {
        var savedUserAccount = userAccountRepository.save(userAccount);

        sendCommunicationTopicProducerService.execute(
            new SendCommunicationMessage(
                savedUserAccount.getId(),
                savedUserAccount.getEmail(),
                "Updated '" + savedUserAccount.getEmail() + "'"
            )
        );

        return savedUserAccount;
    }
}