package com.management.domain.usecases;

import com.management.domain.entities.UserAccount;

public interface CreateUserAccountUseCase {
    UserAccount execute(UserAccount userAccount);
}
