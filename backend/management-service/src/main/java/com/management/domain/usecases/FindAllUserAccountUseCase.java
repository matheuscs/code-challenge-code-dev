package com.management.domain.usecases;

import com.management.domain.entities.UserAccount;
import java.util.List;
import java.util.UUID;

public interface FindAllUserAccountUseCase {
    List<UserAccount> execute();
}
