package com.management.domain.usecases;

import com.management.domain.entities.UserAccount;
import java.util.UUID;

public interface FindUserAccountByIdUseCase {
    UserAccount execute(UUID uuid);
}
