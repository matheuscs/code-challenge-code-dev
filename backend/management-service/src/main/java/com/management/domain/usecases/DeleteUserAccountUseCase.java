package com.management.domain.usecases;

import java.util.UUID;

public interface DeleteUserAccountUseCase {
    void execute(UUID userAccountId);
}
