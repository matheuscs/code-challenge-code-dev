package com.management.dtos.request;

import java.io.Serializable;
import java.util.UUID;

public record CreateUserAccountRequest(
    UUID id,
    String name,
    String email
) implements Serializable {}