package com.management.dtos.response;

import java.io.Serializable;
import java.util.UUID;

public record CreateUserAccountResponse(
    UUID id,
    String name,
    String email
) implements Serializable {}
